﻿using Novotel.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Novotel.DAL
{
	public class CategoryDAL
	{
		SqlConnection sqlcon = new SqlConnection(@"Data Source = (localdb)\MSSQLLocalDB;Initial Catalog = Novotel; Integrated Security = True;");
		
		CategoryAndSubCategory cs = new CategoryAndSubCategory();

		public CategoryAndSubCategory getCatwithSubCat(int catId)
		{
			if (sqlcon.State == ConnectionState.Closed)
			{
				sqlcon.Open();
				SqlCommand sqlCommand = new SqlCommand("getCategoryByCategoryId", sqlcon);
				sqlCommand.CommandType = CommandType.StoredProcedure;
				sqlCommand.Parameters.AddWithValue("@categoryId",catId);
				sqlCommand.ExecuteNonQuery();


				using (SqlDataReader reader = sqlCommand.ExecuteReader())
				{
					// Check is the reader has any rows at all before starting to read.
					if (reader.HasRows)
					{
						// Read advances to the next row.
						while (reader.Read())
						{

							Category c = new Category();


								// To avoid unexpected bugs access columns by name.
								c.id = reader.GetInt32(reader.GetOrdinal("categoryId"));
								c.active = reader.GetBoolean(reader.GetOrdinal("categoryActive"));
								c.name = reader.GetString(reader.GetOrdinal("categoryName"));
								c.image = reader.GetString(reader.GetOrdinal("categoryImage"));
								c.menuId = reader.GetInt32(reader.GetOrdinal("menuid"));
								c.createdBy = reader.GetString(reader.GetOrdinal("createdBy"));
								//c.updatedBy = reader.GetString(reader.GetOrdinal("UpdatedBy"));
								c.createdDate = reader.GetDateTime(reader.GetOrdinal("createdDate"));
								//c.updatedDate = reader.GetDateTime(reader.GetOrdinal("updatedDate"));
								c.deleted = reader.GetBoolean(reader.GetOrdinal("deleted"));

							if (c.deleted == false)
							{
								cs.Categories.Add(c);
								//subCategories.Add(s);
							}

							SubCategory s = new SubCategory();

							// To avoid unexpected bugs access columns by name.
							s.id = reader.GetInt32(reader.GetOrdinal("subcategoryId"));
							s.active = reader.GetBoolean(reader.GetOrdinal("subcategoryActive"));
							s.name = reader.GetString(reader.GetOrdinal("subcategoryName"));
							s.image = reader.GetString(reader.GetOrdinal("subcategoryImage"));
							s.categoryId = reader.GetInt32(reader.GetOrdinal("categoryId"));
							s.createdBy = reader.GetString(reader.GetOrdinal("createdBy"));
							//c.updatedBy = reader.GetString(reader.GetOrdinal("UpdatedBy"));
							s.createdDate = reader.GetDateTime(reader.GetOrdinal("createdDate"));
							//c.updatedDate = reader.GetDateTime(reader.GetOrdinal("updatedDate"));
							s.deleted = reader.GetBoolean(reader.GetOrdinal("deleted"));

							if (s.deleted == false)
							{
								cs.SubCategories.Add(s);
								//subCategories.Add(s);
							}


						}
					}
				}


				




				sqlcon.Close();

				return cs;

			}
			return null;
		}

	}

	public class CategoryAndSubCategory
	{
		public CategoryAndSubCategory()
		{
			Categories = new List<Category>();
			SubCategories = new List<SubCategory>();
		}
		public List<Category> Categories { get; set; }
		public List<SubCategory> SubCategories { get; set; }
		public int chirag = 1;

		//public List<Category> Categories= new List<Category>();
		//public List<SubCategory> SubCategories = new List<SubCategory>();

	}

}
