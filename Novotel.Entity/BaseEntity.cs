﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Novotel.Entity
{
	public class BaseEntity
	{
		public int id { get; set; }
		public string name { get; set; }
		public string image { get; set; }
		public bool active { get; set; }
		public string createdBy { get; set; }
		public string updatedBy { get; set; }
		public bool deleted { get; set; }
		public DateTime createdDate { get; set; }
		public DateTime updatedDate { get; set; }
	}
}
