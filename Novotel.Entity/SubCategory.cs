﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Novotel.Entity
{
	public class SubCategory: BaseEntity
	{
		public List<Item> items { get; set; }
		public int categoryId { get; set; }
	}
}
