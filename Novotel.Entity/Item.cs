﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Novotel.Entity
{
	public class Item : BaseEntity
	{
		public string description { get; set; }
		public decimal price { get; set; }
		public int subCategoryId { get; set; }
	}
}
