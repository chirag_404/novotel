﻿using Novotel.Entity;
using Novotel.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Novotel.Web.Controllers
{
    public class MenuController : ApiController
    {
		static List<Menu> menus = new List<Menu>();
		MenuService menuService = new MenuService();

		/// <summary>
		/// To get all menu data.
		/// </summary>
		/// <returns></returns>
		//[HttpGet]
		//public List<Menu> Get()
		//{

		//	List<Menu> m = menuService.getAllMenu();
		//	return m ;
		//}

		//[HttpGet]
		//public string Get()
		//{
		//	SqlConnection sqlcon = new SqlConnection(@"Data Source = (localdb)\MSSQLLocalDB;Initial Catalog = Novotel; Integrated Security = True;");
		//	if (sqlcon.State == ConnectionState.Closed)
		//	{
		//		sqlcon.Open();

		//		SqlDataAdapter dataAdapter = new SqlDataAdapter("getAllMenus", sqlcon);
		//		dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
		//		DataTable dataTable = new DataTable();
		//		dataAdapter.Fill(dataTable);
		//		sqlcon.Close();


		//	}
		//	return null;
		//}


		[HttpPost]
		public void Post(Menu menu)
		{
			///test testbranch
			menuService.saveMenu(menu);
			
			//this comment is after stash
			//Test in marster merge.
		}

		[HttpPut]
		public void Put(Menu menu)
		{
			menuService.updateMenu(menu);
		}

		[HttpDelete]
		public void Delete(int menuId)
		{
			menuService.deleteInMenu(menuId);
		}


		[HttpGet]
		public AllClass Get()
		{
			/// <summary>
			/// See All Dependency for menu
			/// </summary>
			/// <returns></returns>
			AllClass ac = menuService.SeeMenuDependency();
			return ac;
		}

	}
}
