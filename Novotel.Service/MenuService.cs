﻿using Novotel.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Novotel.Service
{
	/// <summary>
	/// 
	/// </summary>
	public class MenuService
	{
		SqlConnection sqlcon = new SqlConnection(@"Data Source = (localdb)\MSSQLLocalDB;Initial Catalog = Novotel; Integrated Security = True;");
		List<Menu> menus = new List<Menu>();
		AllClass allClass = new AllClass();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="menu"></param>
		public void saveMenu(Menu menu)
		{


			if (sqlcon.State == ConnectionState.Closed)
			{
				sqlcon.Open();
				SqlCommand sqlCommand = new SqlCommand("insertInMenu", sqlcon);
				sqlCommand.CommandType = CommandType.StoredProcedure;

				sqlCommand.Parameters.AddWithValue("@menuActive", menu.active);
				sqlCommand.Parameters.AddWithValue("@menuName", menu.name);
				sqlCommand.Parameters.AddWithValue("@menuImage", menu.image);
				sqlCommand.Parameters.AddWithValue("@menuMode", menu.menuMode);
				sqlCommand.Parameters.AddWithValue("@createdBy", menu.createdBy);


				sqlCommand.ExecuteNonQuery();
				sqlcon.Close();

			}
		}

		public List<Menu> getAllMenu()
		{
			if (sqlcon.State == ConnectionState.Closed)
			{
				sqlcon.Open();
				SqlCommand sqlCommand = new SqlCommand("getAllMenus", sqlcon);
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlDataAdapter dataAdapter = new SqlDataAdapter();
				dataAdapter.SelectCommand = sqlCommand;
				DataSet dataSet = new DataSet();
				dataAdapter.Fill(dataSet,"menu");
				SqlDataReader reader = sqlCommand.ExecuteReader();

				foreach (DataRow prow in dataSet.Tables["menu"].Rows)
				{
					Menu m = new Menu();
					m.id = reader.GetInt32(reader.GetOrdinal("menuId"));
					m.name = reader.GetString(reader.GetOrdinal("menuName"));
					menus.Add(m);
				}
				




				//using (SqlDataReader reader = sqlCommand.ExecuteReader())
				//{
				//	// Check is the reader has any rows at all before starting to read.
				//	if (reader.HasRows)
				//	{
				//		// Read advances to the next row.
				//		while (reader.Read())
				//		{

				//			Menu p = new Menu();

				//			// To avoid unexpected bugs access columns by name.
				//			p.id = reader.GetInt32(reader.GetOrdinal("menuId"));
				//			p.active = reader.GetBoolean(reader.GetOrdinal("menuActive"));
				//			p.name = reader.GetString(reader.GetOrdinal("menuName"));
				//			p.image = reader.GetString(reader.GetOrdinal("menuImage"));
				//			p.menuMode = reader.GetString(reader.GetOrdinal("menuMode"));
				//			p.createdBy = reader.GetString(reader.GetOrdinal("createdBy"));
				//			p.deleted = reader.GetBoolean(reader.GetOrdinal("deleted"));
				//			//p.updatedBy = reader.GetString(reader.GetOrdinal("UpdatedBy"));
				//			p.createdDate = reader.GetDateTime(reader.GetOrdinal("createdDate"));
				//			//p.updatedDate = reader.GetDateTime(reader.GetOrdinal("updatedDate"));

				//			if (p.deleted == false)
				//			{
				//				menus.Add(p);
				//			}


				//		}
				//	}
				//}



				sqlcon.Close();

				return menus;

			}
			return null;
		}

		public void updateMenu(Menu menu)
		{

			if (sqlcon.State == ConnectionState.Closed)
			{
				sqlcon.Open();
				SqlCommand sqlCommand = new SqlCommand("updateInMenu", sqlcon);
				sqlCommand.CommandType = CommandType.StoredProcedure;

				sqlCommand.Parameters.AddWithValue("@menuId", menu.id);
				sqlCommand.Parameters.AddWithValue("@menuActive", menu.active);
				sqlCommand.Parameters.AddWithValue("@menuName", menu.name);
				sqlCommand.Parameters.AddWithValue("@menuImage", menu.image);
				sqlCommand.Parameters.AddWithValue("@menuMode", menu.menuMode);
				sqlCommand.Parameters.AddWithValue("@updatedBy", menu.updatedBy);

				sqlCommand.ExecuteNonQuery();
				sqlcon.Close();
			}
		}

		public void deleteInMenu(int menuId)
		{
			if (sqlcon.State == ConnectionState.Closed)
			{
				sqlcon.Open();
				SqlCommand sqlCommand = new SqlCommand("deleteInMenu", sqlcon);
				sqlCommand.CommandType = CommandType.StoredProcedure;
				sqlCommand.Parameters.AddWithValue("@menuId", menuId);
				sqlCommand.ExecuteNonQuery();
				sqlcon.Close();



			}
		}

		public AllClass SeeMenuDependency()
		{
			if (sqlcon.State == ConnectionState.Closed)
			{
				sqlcon.Open();
				SqlCommand sqlCommand = new SqlCommand("seedependency", sqlcon);
				sqlCommand.CommandType = CommandType.StoredProcedure;
				
				sqlCommand.ExecuteNonQuery();


				using (SqlDataReader reader = sqlCommand.ExecuteReader())
				{
					// Check is the reader has any rows at all before starting to read.
					if (reader.HasRows)
					{
						// Read advances to the next row.
						while (reader.Read())
						{
							MenuPart m = new MenuPart
							{
								// To avoid unexpected bugs access columns by name.
								id = reader.GetInt32(reader.GetOrdinal("menuid")),
								name = reader.GetString(reader.GetOrdinal("menuName"))
							};
							allClass.menus.Add(m);

							CategoryPart c = new CategoryPart();
							// To avoid unexpected bugs access columns by name.
							
							try
							{
								c.id = reader.GetInt32(reader.GetOrdinal("categoryId"));
								c.name = reader.GetString(reader.GetOrdinal("categoryName"));
								allClass.categories.Add(c);
								m.categoryParts.Add(c);
							}
							catch (Exception )
							{
								c = null;
								allClass.categories.Add(c);
								m.categoryParts.Add(c);
								continue;

							}

							


							SubCatPart s = new SubCatPart();
							
							try
							{
								s.id = reader.GetInt32(reader.GetOrdinal("subcategoryId"));
								s.name = reader.GetString(reader.GetOrdinal("subcategoryName"));
								allClass.subCategories.Add(s);
								c.subCatParts.Add(s);
							}
							catch (Exception )
							{
								s = null;
								allClass.subCategories.Add(s);
								c.subCatParts.Add(s);
								continue;

							}
							

							ItemPart i = new ItemPart();
							
							// To avoid unexpected bugs access columns by name.

							try
							{
								i.id = reader.GetInt32(reader.GetOrdinal("itemid"));
								i.name = reader.GetString(reader.GetOrdinal("itemname"));
								allClass.items.Add(i);
								s.itemParts.Add(i);
							}
							catch (Exception )
							{
								i = null; 
								allClass.items.Add(i);
								s.itemParts.Add(i);

							}

							

						}
					}
				}







				sqlcon.Close();

				return allClass;

			}
			return null;

		}
	}

	public class AllClass
	{
		public AllClass()
		{
			menus = new List<MenuPart>();
			categories = new List<CategoryPart>();
			subCategories = new List<SubCatPart>();
			items = new List<ItemPart>();
		}

		public List<MenuPart> menus { get; set; }
		public List<CategoryPart> categories { get; set; }
		public List<SubCatPart> subCategories { get; set; }
		public List<ItemPart> items { get; set; }

	}

	public class MenuPart {
		public int? id { get; set; }
		public string name { get; set; }
		public List<CategoryPart> categoryParts = new List<CategoryPart>();

	
		
	}
	public class CategoryPart
	{
		public int? id { get; set; }
		public string name { get; set; }
		public List<SubCatPart> subCatParts = new List<SubCatPart>();
	}
	public class SubCatPart
	{
		public int? id { get; set; }
		public string name { get; set; }
		public List<ItemPart> itemParts = new List<ItemPart>();
	}
	public class ItemPart
	{
		public int? id { get; set; }
		public string name { get; set; }
	}

}